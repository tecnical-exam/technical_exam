import { useState, useEffect } from 'react';

import LaunchCard from './LaunchCard'

export default function LaunchView({launchProp}) {

	const [launchArr, setLaunchArr] = useState([])

	useEffect(() => {
		const launches = launchProp.map(name => {
				return <LaunchCard key={launchProp.flight_number} launchProp={name} />	
		})

		setLaunchArr(launches)

	}, [launchProp])

	return(
		<>
			{launchArr}
		</>
	)
}
