
import {Card} from 'react-bootstrap';


export default function LaunchCard({launchProp}) {

	
	const { launchpad, name, flight_number} = launchProp
	return(
		<>
			<Card className = "d-inline-flex m-2 shadow md={4}"style={{width: '18rem'}} >
			  <Card.Img variant="top" src="" />
			  <Card.Body>
			    <Card.Title >{launchpad}</Card.Title>
			    <Card.Title >{name}</Card.Title>
			    <Card.Title >{flight_number}</Card.Title>
			  </Card.Body>
			</Card>

			
		</>
		)

}





