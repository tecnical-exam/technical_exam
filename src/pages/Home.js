import {useState, useEffect} from 'react';
import LaunchView from  "../components/LaunchView"

export default function Home(){

	
	const [launchData, setLaunchData] = useState([])


	const fetchData = () => {
		fetch(`https://api.spacexdata.com/v4/launches/`)
		.then(res => res.json())
		.then(data => {
			setLaunchData(data)
		})
	}

	useEffect(() => {
			fetchData()
 	}, [])

	return(
			<div>
					<LaunchView nameProp={launchData}/>
			</div>
		)
}